﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandling : MonoBehaviour
{
    private Transform tf;
    private SpriteRenderer myRenderer;
    private Vector3 startPosition;
    //Array for color swapping + the colors
    private Color[] shipColors = new Color[4];
    private Color blue = new Color(0, 0, 1, 1);
    private Color red = new Color(1, 0, 0, 1);
    private Color white = new Color(1, 1, 1, 1);
    private Color green = new Color(0, 1, 0, 1);
    //Counter for the array so it wil iterate when color change key is pressed
    private int colorCounter = 0;
    //Speed for designer to change
    public float speed;
    //Check for the shift key being pressed
    private bool shiftIsPressed;

    // Start is called before the first frame update
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        tf = GetComponent<Transform>();
        //Set startPosition to the initially rendered position
        startPosition = this.transform.position;
        //Assign each color to an array value
        shipColors[0] = blue;
        shipColors[1] = red;
        shipColors[2] = white;
        shipColors[3] = green;

    }

    // Update is called once per frame
    void Update()
    {
        //Flag to check if the shift key is being held
        if (Input.GetKey(KeyCode.LeftShift))
        {
            shiftIsPressed = true;
        }
            if (shiftIsPressed == true)
            {
                //If the shift key is being held, only move a single unit
                if (Input.GetKeyDown(KeyCode.W))
                {
                    tf.position += Vector3.up;
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    tf.position += Vector3.left;
                }
                else if (Input.GetKeyDown(KeyCode.S))
                {
                    tf.position += Vector3.down;
                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    tf.position += Vector3.right;
                }

                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    tf.position += Vector3.up;
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    tf.position += Vector3.left;
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    tf.position += Vector3.down;
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    tf.position += Vector3.right;
                }
            }      
        }
        //Disable flag when shift is released
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            shiftIsPressed = false;
        }
        
        if(shiftIsPressed == false)
        {
            //Horizontal/Vertical Movement with WASD
            if (Input.GetKey(KeyCode.W))
            {
                tf.position += (Vector3.up * speed);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                tf.position += (Vector3.left * speed);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                tf.position += (Vector3.down * speed);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                tf.position += (Vector3.right * speed);
            }

            //Horizontal/Vertical Movement with Arrows
            if (Input.GetKey(KeyCode.UpArrow))
            {
                tf.position += (Vector3.up * speed);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                tf.position += (Vector3.left * speed);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                tf.position += (Vector3.down * speed);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                tf.position += (Vector3.right * speed);
            }

            //WASD Diagonal Movement
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
            {
                tf.position += (new Vector3(-1, 1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            {
                tf.position += (new Vector3(1, 1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
            {
                tf.position += (new Vector3(-1, -1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            {
                tf.position += (new Vector3(1, -1, 0) * speed);
            }

            //Arrow Keys Diagonal Movement
            if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftArrow))
            {
                tf.position += (new Vector3(-1, 1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                tf.position += (new Vector3(1, 1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftArrow))
            {
                tf.position += (new Vector3(-1, -1, 0) * speed);
            }
            else if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                tf.position += (new Vector3(1, -1, 0) * speed);
            }
        }

    //Resets the players position 
    if (Input.GetKey(KeyCode.Space))
    {
        transform.position = startPosition;
    }

    //Changes sprite color per key press
    if(Input.GetKeyDown(KeyCode.E))
    {
        colorCounter++;
        myRenderer.color = shipColors[colorCounter];
        //When the end of the array is hit, reset to the value before the original. This allows the array to loop as intended.
        if(colorCounter >= 3)
        {
            colorCounter = -1;
        }
    }
    }
}
