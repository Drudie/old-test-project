﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    private InputHandling myInput;

    // Start is called before the first frame update
    void Start()
    {
        myInput = GetComponent<InputHandling>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            myInput.enabled = !myInput.enabled;
        }
    }
}
